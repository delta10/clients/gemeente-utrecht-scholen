import './normalize.css'
import '@utrecht/design-tokens/dist/index.css'
import '@utrecht/component-library-css/dist/bem.css'
import 'tippy.js/themes/light-border.css'
import 'tippy.js/animations/shift-away-subtle.css'

import { createApp } from 'vue'
import VueDebounce from 'vue-debounce'
import { plugin as VueTippy } from 'vue-tippy'

import App from './App.vue'

const app = createApp(App)

app.use(VueDebounce)
app.use(VueTippy, {
    directive: 'tippy', // => v-tippy
    component: 'tippy', // => <tippy/>
    componentSingleton: 'tippy-singleton', // => <tippy-singleton/>
    defaultProps: {
        interactive: true,
        offset: [0, 5],
        placement: 'top',
        duration: [200, 175],
        delay: [1000, 0],
        theme: 'dark',
        animation: 'shift-away-subtle',
        arrow: false,
        popperOptions: {
            strategy: 'fixed',
            modifiers: [
                {
                    name: 'preventOverflow',
                    options: {
                        altAxis: true,
                        tether: false,
                    },
                },
            ],
        },
    },
})

app.mount('#app')
