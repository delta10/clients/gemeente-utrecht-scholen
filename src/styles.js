import stringDivider from './utils/stringDivider'
import { Icon, Style, Fill, Stroke, Circle, Text } from 'ol/style'

export const homeStyle = new Style({
    image: new Icon({
        src: 'images/marker-home.svg',
        anchor: [0.55, 42],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
    }),
})

export const schoolStyle = new Style({
    image: new Icon({ src: 'images/marker-school.svg' }),
    zIndex: 1,
})

export const schoolSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-school-selected.svg' }),
    zIndex: 2,
})

export const schoolStedelijkStyle = new Style({
    image: new Icon({ src: 'images/marker-school-stedelijk.svg' }),
    zIndex: 1,
})

export const schoolStedelijkSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-school-stedelijk-selected.svg' }),
    zIndex: 2,
})

export const schoolPreferenceStyle = new Style({
    image: new Icon({ src: 'images/marker-school-preference.svg' }),
    zIndex: 1,
})

export const schoolPreferenceSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-school-preference-selected.svg' }),
    zIndex: 2,
})

export const getStyle = (map, style, feature, selected) => {
    if (style === 'home') {
        return homeStyle
    }

    if (style === 'school') {
        if (feature.get('highlighted')) {
            return selected ? schoolPreferenceSelectedStyle : schoolPreferenceStyle
        }

        if (feature.get('stedelijke_school') === 'Ja') {
            return selected ? schoolStedelijkSelectedStyle : schoolStedelijkStyle
        }

        return selected ? schoolSelectedStyle : schoolStyle
    }
}
