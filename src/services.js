let apiBaseUrl = 'https://acc-datastore.utrecht.onatlas.nl/api/v1/datasets'
if (window.location.hostname === 'naardebasisschool.utrecht.onatlas.nl') {
    apiBaseUrl = 'https://datastore.utrecht.onatlas.nl/api/v1/datasets'
}

export const getSchools = async () => {
    const result = await fetch(`${apiBaseUrl}/scholen`)
    const jsonData = await result.json()

    return jsonData.records.map((school) => {
        return {
            ...school,
            geometry: school.geometry,
        }
    })
}

export const getPrioritySchools = async (nummeraanduidingId) => {
    const result = await fetch(`${apiBaseUrl}/schoolroutes/${nummeraanduidingId}`)
    const jsonData = await result.json()

    return [jsonData.schoolid1, jsonData.schoolid2, jsonData.schoolid3, jsonData.schoolid4]
}
